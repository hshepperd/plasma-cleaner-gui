# A simple GUI to control the motion of an XYZ stage with a plasma cleaner.
# The motion is always in a square snaking pattern, starting at the front right corner of the table, moving left (-X) for the set width, then back (+Y) for the set spacing, then right (+X) for the set width, then back (+Y) for the set spacing, etc, until the entire area has been covered.
# If the depth (Y) is greater than the width (X), then the motion is the same, but the depth (Y) is done first, and the spacing is over the depth (Y).

# ESC - Quit

import os
import serial
import serial.tools.list_ports
import sys
import glob
import grbl
import tkinter as tk
import tkinter.ttk as ttk
import threading
import time

version = "0.1.1"

# Width is the X-axis
width_min = 10
width_max = 350
width_default = 100

# Depth is the Y-axis
depth_min = 10
depth_max = 350
depth_default = 100

# Height is the Z-axis
height_min = 0
height_max = 130
height_default = 100

# Spacing is the distance between each left/right or front/back pass.
spacing_min = 1
spacing_max = 40
spacing_default = 10

# Feedrate is the speed of the plasma cleaner in mm/min.
feedrate_min = 1
feedrate_max = 30000
feedrate_default = 5000

# Passes is the number of times the plasma cleaner will go over the same area.
passes_min = 1
passes_max = 500
passes_default = 1

# Safe height is the height the plasma cleaner will move to when not cleaning.
safe_height = 150

def main():
    plasma = grbl.Grbl()

    window = tk.Tk()
    window.title("Plasma Cleaner")
    window.bind('<Escape>', lambda e: window.quit())

    frame = tk.Frame(window)
    frame.pack(fill=tk.BOTH, side=None, expand=False)

    port_list = get_ports()

    port_value = tk.StringVar()
    # TODO Save last used port to file.
    port_value.set(port_list[0] if len(port_list) > 0 else "")

    def port_combobox_update_function(event):
        ports = get_ports()
        port_combobox['values'] = ports
        if len(ports) > 0:
            port_combobox.current(0)

    port_combobox = ttk.Combobox(frame, values=port_list, state="readonly", textvariable=port_value, width=40)
    port_combobox.bind("<Enter>", port_combobox_update_function)
    port_combobox.pack(fill=tk.X, padx=10, pady=(5, 2))

    def connect_function():
        port = port_value.get()

        if plasma.is_connected():
            plasma.disconnect()
            message_area["text"] = "Disconnected from {}".format(port)
            connect_button["text"] = "Connect"
            port_combobox["state"] = "readonly"
            home_button["state"] = tk.DISABLED
            start_button["state"] = tk.DISABLED
        else:
            if port != "":
                if plasma.connect(port):
                    message_area["text"] = "Connected to {}".format(port)
                    connect_button["text"] = "Disconnect"
                    port_combobox["state"] = tk.DISABLED
                    home_button["state"] = tk.NORMAL
                    start_button["state"] = tk.NORMAL
                else:
                    message_area["text"] = "Failed to connect to {}".format(port)
            else:
                message_area["text"] = "No port selected"

    connect_button = ttk.Button(frame, text="Connect", command=connect_function)
    connect_button.pack(fill=tk.X, padx=10, pady=(2, 8))

    ttk.Separator(frame, orient=tk.HORIZONTAL).pack(fill=tk.X, padx=10, pady=8)

    def home_function():
        if plasma.is_connected():
            message_area["text"] = "Homing..."

            def end_homing(success):
                progressbar.stop()

                if not plasma.is_connected():
                    message_area["text"] = "Disconnected"
                    return

                if success:
                    message_area["text"] = "Homing complete"
                    start_button["state"] = tk.NORMAL
                else:
                    message_area["text"] = "Homing failed"

                home_button["state"] = tk.NORMAL

            def start_homing():
                success = plasma.home()
                window.after(100, end_homing, success)

            homing_thread = threading.Thread(target=start_homing)
            homing_thread.start()

            home_button["state"] = tk.DISABLED
            start_button["state"] = tk.DISABLED

            progressbar.start()
        else:
            message_area["text"] = "Not connected"

    home_button = ttk.Button(frame, text="Home", command=home_function)
    home_button.pack(fill=tk.X, padx=10, pady=8)
    home_button["state"] = tk.DISABLED

    ttk.Separator(frame, orient=tk.HORIZONTAL).pack(fill=tk.X, padx=10, pady=8)

    def spinbox_focus_out_function(event):
        widget = event.widget

        try:
            val = int(float(widget.get()))
            val = min(widget["to"], max(widget["from"], val))
        except ValueError:
            val = widget.default

        widget.set(val)

    width_label = ttk.Label(frame, text="Width (X) (mm)")
    width_label.pack(fill=tk.X, padx=10, pady=(8, 0))

    width_spinbox = ttk.Spinbox(frame, from_=width_min, to=width_max, increment=10)
    width_spinbox.set(width_default)
    width_spinbox.default = width_default
    width_spinbox.bind("<FocusOut>", spinbox_focus_out_function)
    width_spinbox.pack(fill=tk.X, padx=10, pady=(0, 8))

    depth_label = ttk.Label(frame, text="Depth (Y) (mm)")
    depth_label.pack(fill=tk.X, padx=10, pady=(8, 0))

    depth_spinbox = ttk.Spinbox(frame, from_=depth_min, to=depth_max, increment=10)
    depth_spinbox.set(depth_default)
    depth_spinbox.default = depth_default
    depth_spinbox.bind("<FocusOut>", spinbox_focus_out_function)
    depth_spinbox.pack(fill=tk.X, padx=10, pady=(0, 8))

    height_label = ttk.Label(frame, text="Height (Z) (mm)")
    height_label.pack(fill=tk.X, padx=10, pady=(8, 0))

    height_spinbox = ttk.Spinbox(frame, from_=height_min, to=height_max, increment=1)
    height_spinbox.set(height_default)
    height_spinbox.default = height_default
    height_spinbox.bind("<FocusOut>", spinbox_focus_out_function)
    height_spinbox.pack(fill=tk.X, padx=10, pady=(0, 8))

    spacing_label = ttk.Label(frame, text="Spacing (mm)")
    spacing_label.pack(fill=tk.X, padx=10, pady=(8, 0))

    spacing_spinbox = ttk.Spinbox(frame, from_=spacing_min, to=spacing_max, increment=1)
    spacing_spinbox.set(spacing_default)
    spacing_spinbox.default = spacing_default
    spacing_spinbox.bind("<FocusOut>", spinbox_focus_out_function)
    spacing_spinbox.pack(fill=tk.X, padx=10, pady=(0, 8))

    feedrate_label = ttk.Label(frame, text="Feedrate (mm/min)")
    feedrate_label.pack(fill=tk.X, padx=10, pady=(8, 0))

    feedrate_spinbox = ttk.Spinbox(frame, from_=feedrate_min, to=feedrate_max, increment=100)
    feedrate_spinbox.set(feedrate_default)
    feedrate_spinbox.default = feedrate_default
    feedrate_spinbox.bind("<FocusOut>", spinbox_focus_out_function)
    feedrate_spinbox.pack(fill=tk.X, padx=10, pady=(0, 8))

    passes_label = ttk.Label(frame, text="Passes")
    passes_label.pack(fill=tk.X, padx=10, pady=(8, 0))

    passes_spinbox = ttk.Spinbox(frame, from_=passes_min, to=passes_max, increment=1)
    passes_spinbox.set(passes_default)
    passes_spinbox.default = passes_default
    passes_spinbox.bind("<FocusOut>", spinbox_focus_out_function)
    passes_spinbox.pack(fill=tk.X, padx=10, pady=(0, 8))

    ttk.Separator(frame, orient=tk.HORIZONTAL).pack(fill=tk.X, padx=8, pady=8)

    def start_function():
        if not plasma.is_connected():
            message_area["text"] = "Not connected"
            return

        area_w = int(float(width_spinbox.get()))
        area_h = int(float(depth_spinbox.get()))
        # area_x = width_max - area_w - 1
        area_x = width_max - area_w
        # area_x = 0
        area_y = 0
        spacing = int(float(spacing_spinbox.get()))
        height = int(float(height_spinbox.get()))
        feedrate = int(float(feedrate_spinbox.get()))
        passes = int(float(passes_spinbox.get()))
        # print("Start: {} {} {} {} {} {} {}".format(area_w, area_h, area_x, area_y, spacing, height, feedrate))

        # print(area_x)

        def end_scan():
            state = plasma.get_state()

            if state == "idle":
                message_area["text"] = "Done"
            else:
                message_area["text"] = "Error: Scan failed"

            progressbar.stop()
            home_button["state"] = tk.NORMAL
            width_spinbox["state"] = tk.NORMAL
            depth_spinbox["state"] = tk.NORMAL
            height_spinbox["state"] = tk.NORMAL
            spacing_spinbox["state"] = tk.NORMAL
            feedrate_spinbox["state"] = tk.NORMAL
            passes_spinbox["state"] = tk.NORMAL
            start_button["state"] = tk.NORMAL
            # stop_button["state"] = tk.DISABLED
            # cube_button["state"] = tk.NORMAL

        def start_scan():
            state = plasma.get_state()

            if state is None:
                print("Error: Plasma state is unknown")
                window.after(100, end_scan)
                return

            if state == "run":
                print("Error: Plasma is already running")
                window.after(100, end_scan)
                return

            if state == "alarm":
                message_area["text"] = "Homing..."
                if not plasma.home():
                    print("Error: Plasma failed to home")
                    window.after(100, end_scan)
                    return

            for i in range(passes):
                message_area["text"] = f"Scanning... (Pass {i+1}/{passes})"

                scan_area(plasma, area_w, area_h, area_x, area_y, spacing, height, feedrate)

                while plasma.get_state() == "run":
                    time.sleep(0.2)

            plasma.feedrate(5000)
            plasma.goto(None, None, safe_height)
            plasma.goto(width_max, depth_max, None)

            while plasma.get_state() == "run":
                time.sleep(0.2)

            window.after(100, end_scan)

        home_button["state"] = tk.DISABLED
        width_spinbox["state"] = tk.DISABLED
        depth_spinbox["state"] = tk.DISABLED
        height_spinbox["state"] = tk.DISABLED
        spacing_spinbox["state"] = tk.DISABLED
        feedrate_spinbox["state"] = tk.DISABLED
        passes_spinbox["state"] = tk.DISABLED
        start_button["state"] = tk.DISABLED
        # stop_button["state"] = tk.NORMAL
        # cube_button["state"] = tk.DISABLED
        # message_area["text"] = "Scanning..."
        progressbar["value"] = 0

        progressbar.start()

        scan_thread = threading.Thread(target=start_scan)
        scan_thread.start()

    start_button = ttk.Button(frame, text="Start", command=start_function)
    start_button.pack(fill=tk.X, padx=10, pady=(6, 4))
    start_button["state"] = tk.DISABLED

    ttk.Separator(frame, orient=tk.HORIZONTAL).pack(fill=tk.X, padx=8, pady=8)

    progressbar = ttk.Progressbar(frame, orient=tk.HORIZONTAL, length=100, mode="indeterminate")
    progressbar.pack(fill=tk.X, padx=10, pady=(4, 4))

    message_area = ttk.Label(frame, text="")
    message_area.pack(fill=tk.BOTH, padx=10, pady=1)

    version_label = ttk.Label(frame, text="v{}".format(version))
    version_label.pack(side=tk.RIGHT, padx=10, pady=1)

    window.mainloop()

    plasma.disconnect()

def scan_area(plasma, area_w, area_h, area_x, area_y, spacing, height, feedrate):
    plasma.feedrate(5000)

    plasma.goto(None, None, safe_height)

    if area_w >= area_h:
        plasma.goto(area_x + area_w, area_y) # Move to starting position
        plasma.goto(None, None, height) # Move to cutting height

        plasma.feedrate(feedrate)

        number_of_passes = int(area_h / spacing + 0.499)

        direction = -1

        for i in range(number_of_passes):
            plasma.move(area_w * direction, 0)
            plasma.move(0, spacing)
            direction *= -1

        plasma.move(area_w * direction, 0)
    else:
        plasma.goto(area_x + area_w, area_y) # Move to starting position
        plasma.goto(None, None, height) # Move to cutting height

        plasma.feedrate(feedrate)

        number_of_passes = int(area_w / spacing + 0.499)

        direction = 1

        for i in range(number_of_passes):
            plasma.move(0, area_h * direction)
            plasma.move(-spacing, 0)
            direction *= -1

        plasma.move(0, area_h * direction)

def get_ports():
    ports = serial.tools.list_ports.comports()

    return [port.device for port in ports]

if __name__ == "__main__":
    sys.stderr = open("stderr.txt", "w")

    main()
