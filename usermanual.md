# Plasma Cleaner GUI User Manual

## Introduction
The Plasma Cleaner GUI is a graphical user interface designed to control the motion of an XYZ stage with a plasma cleaner. The motion follows a square snaking pattern, covering the area specified by the width and depth parameters. The GUI allows you to set parameters such as width, depth, height, spacing, feedrate, and number of passes to customize the cleaning process.

## Safety
- Before using the Plasma Cleaner GUI, make sure you have read and understood the user manuals for the plasma and polymerization units.
- If in doubt, press the emergency stop (E-Stop) button on the XYZ stage.
- The plasma unit, polymerization unit, and xyz stage will stop when the E-Stop is pressed or the door is opened.
- The E-Stop on the Plasma Unit will NOT stop the XYZ stage.
- Do not touch the stage or any moving parts while the motion is in progress.
- If required, the stage can be moved manually by hand y first pressing the E-Stop button.
- Be cautious of the flexible covers on the X, Y, and Z axes and avoid pressing on them during manual operation.
- The Z-axis has a brake and cannot be moved manually.
- Always ensure the fume extractor is on when using the plasma cleaner.

## General Operation
1. Turn the plasma and polymerization units on as per the user manuals.
1. Turn the fume extractor on.
1. Turn the xyz stage on.
1. Start the Plasma Cleaner GUI.
1. Connect the xyz stage USB port to the computer running the Plasma Cleaner GUI.
1. Place the part to be cleaned in the front right corner of the plate, leaving a 30mm margin from the edge.
1. Measure the maximum height of the part to be cleaned and enter it in to the "Height" spinbox.
1. Ensure the door is closed, the e-stop button is not pressed, and the green beacon is on.
1. Press the "Connect" button in the GUI.
1. Set the desired parameters for width, depth, spacing, feedrate, and number of passes.
1. Ensure the plasma and polymerization units are ready.
1. Press the "Start" button to start the cleaning process. (The XYZ stage will home automatically if it has not already been homed.)
1. The plasma cleaner will stop when the motion is complete or when the E-Stop button is pressed or the door is opened.

## GUI Operation
- The GUI consists of several elements:
    - Port selection: Choose the appropriate port for the plasma cleaner.
    - Connect button: Establish a connection with the plasma cleaner.
    - Home button: Home the XYZ stage.
    - Width, depth, height, spacing, feedrate, and passes spinboxes: Set the desired parameters for the cleaning process.
    - Start button: Start the cleaning process.
    - Progress bar: Indicates that cleaning is in progress.
    - Message area: Displays status messages and error messages.
    - Version label: Displays the version number of the GUI.
- The ESC button closes the GUI.

## Beacon Status
- The green beacon indicates that the plasma cleaner is ready.
- The red beacon indicates that the E-Stop is pressed.
- The amber beacon indicates that the plasma unit is running.

## Troubleshooting
- If in doubt, press and release the E-Stop to reset the XYZ stage controller.
- If the GUI fails to connect, ensure that:
    - The USB cable is connected
    - The correct port is selected
    - The door is closed
    - The E-Stop button is not pressed
    - The green beacon is on
- If the plasma cleaner fails to home, it may be over one of the limit switches. Pressing "Home" 4 or 5 times may get it off the limit switch, or the stage can be manually move by first pressing the E-Stop.
- If the plasma cleaner fails to start the cleaning process, check that the green beacon on the plasma cleaner is on and that the XYZ stage is properly homed.
- If the cleaning process stops unexpectedly, check for any error messages in the message area and ensure that the E-Stop button has not been pressed or the door has not been opened.
- If you encounter any other issues or errors, refer to the user manuals for the plasma cleaner and the polymerization unit or contact technical support for assistance.

## Custom G-Code Programs
- The XYZ stage can accept custom g-code programs via the USB port.
- For more information refer to the <https://github.com/gnea/grbl/wiki/>
- Use caution when running custom g-code programs and monitor the process closely for any unexpected behavior or errors.
