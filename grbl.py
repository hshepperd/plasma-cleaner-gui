import serial
import sys
import time
import numpy as np
import math

pi = 3.141592653589793

# Settings and sample values     Description
# $0=10     Step pulse, microseconds
# $1=25     Step idle delay, milliseconds
# $2=0     Step port invert, mask
# $3=0     Direction port invert, mask
# $4=0     Step enable invert, boolean
# $5=0     Limit pins invert, boolean
# $6=0     Probe pin invert, boolean
# $10=1     Status report, mask
# $11=0.010     Junction deviation, mm
# $12=0.002     Arc tolerance, mm
# $13=0     Report inches, boolean
# $20=0     Soft limits, boolean
# $21=0     Hard limits, boolean
# $22=1     Homing cycle, boolean
# $23=0     Homing dir invert, mask
# $24=25.000     Homing feed, mm/min
# $25=500.000     Homing seek, mm/min
# $26=250     Homing debounce, milliseconds
# $27=1.000     Homing pull-off, mm
# $30=1000.     Max spindle speed, RPM
# $31=0.     Min spindle speed, RPM
# $32=0     Laser mode, boolean
# $100=250.000     X steps/mm
# $101=250.000     Y steps/mm
# $102=250.000     Z steps/mm
# $110=500.000     X Max rate, mm/min
# $111=500.000     Y Max rate, mm/min
# $112=500.000     Z Max rate, mm/min
# $120=10.000     X Acceleration, mm/sec^2
# $121=10.000     Y Acceleration, mm/sec^2
# $122=10.000     Z Acceleration, mm/sec^2
# $130=200.000     X Max travel, mm
# $131=200.000     Y Max travel, mm
# $132=200.000     Z Max travel, mm

class Grbl:
    def __init__(self):
        # self.s = None
        self.s = serial.Serial(port = None, timeout = 0.5)
        self.absolute = None
        self.position = [0, 0, 0]
        # self.homing = False

    def connect(self, port, baud = 115200):
        # self.s = None

        # try:
        # self.s = serial.Serial(port, baudrate = baud, timeout = 1)
        self.s.port = port
        self.s.baudrate = baud
        try:
            self.s.open()
        except serial.serialutil.SerialException:
            # print("Error: Could not open port:", port)
            return False
        # except serial.serialutil.SerialException:
        #     print("Error: Could not open port:", port)
        #     return False

        self.absolute = None
        self.position = [0, 0, 0]
        # self.homing = False

        # time.sleep(1) # Wait for grbl to initialize

        ## Grbl should return: "\r\nGrbl 1.1h ['$' for help]\r\n[MSG:'$H'|'$X' to unlock]\r\n"

        # line = self.s.readline();
        # line = self.s.readline();

        # if line[:5] != b"Grbl ":
        #     self.s.close()
        #     # self.s = None
        #     # raise Exception("Error: Invalid response received.")
        #     print("Error: Invalid response received.")
        #     return False

        # # self.s.reset_input_buffer()
        # self.s.write(b"\n")
        # self.s.timeout = 0.1
        # response = self.s.readline().strip()
        # self.s.timeout = None

        # if response == b"ok":
        #     return True
        # else:
        #     self.s.close()
        #     # print("Error: Wrong response:", response)
        #     return False

        self.s.reset_input_buffer()

        self.reset()
        # self.s.timeout = 0.1
        # response = self.s.readline().strip()
        # response = self.s.readline().strip()
        response = self.s.readlines()
        # self.s.timeout = None

        # print(response)

        if response[1][:5] == b"Grbl ":
            # self.s.reset_input_buffer()
            return True
        else:
            self.s.close()
            # print("Error: Wrong response:", response)
            return False

        # grbl_version = line[5:]
        # grbl_version = grbl_version[:grbl_version.find(b" ")].decode("utf-8")

        # return True

        # print("Connected to Grbl version:", grbl_version)

    def is_connected(self):
        return self.s.is_open

    def disconnect(self):
        # if self.s is not None:
        self.s.close();
        # self.s = None

    def reset(self):
        self.s.write(b"\x18")
        self.s.flush()

    # def test(self):
    #     self.send("S300")
    #     self.send("M3")
    #     self.send("G4 P5")
    #     self.send("M5")

    def get_state(self):
        # if self.homing:
        #     return "homing"

        if self.s.in_waiting > 0:
            # TODO How to handle this? Maybe loop "readline" until empty?
            print("Warning: Input buffer not empty.")

        self.s.reset_input_buffer()

        self.s.write(b"?")
        response = self.s.readline()

        # if response == b"ok\r\n":
        #     return "ok"

        if response[:6] == "ALARM:":
            return "alarm"

        if response[:1] != b"<":
            # print("Error: Invalid response received.")
            return None

        response = response.decode("utf-8").strip()

        response = response[1:-1]

        response = response.split("|")

        # print(response)

        return response[0].lower()

        # first_pipe_index = response.find("|")

    def set_acceleration(self, acceleration):
        return self.send(f"$120={acceleration}") and self.send(f"$121={acceleration}") and self.send(f"$122={acceleration}")

    def set_max_speed(self, speed):
        return self.send(f"$110={speed}") and self.send(f"$111={speed}") and self.send(f"$112={speed}")

    def set_x_steps_per_mm(self, n):
        return self.send(f"$100={n:.3f}\n")

    def set_y_steps_per_mm(self, n):
        return self.send(f"$101={n:.3f}\n")

    def set_z_steps_per_mm(self, n):
        return self.send(f"$102={n:.3f}\n")

    def get_config(self):
        self.s.write(b"\n")
        time.sleep(0.1)
        self.s.reset_input_buffer()

        self.s.write(b"$$\n")
        time.sleep(0.1)
        response = self.s.readlines()

        l = len(response)

        if l != 35:
            print(f"Error: Wrong number of lines received: {l}")
            return None

        config = {}

        for line in response:
            line = line.strip().decode("utf-8")

            if line == "ok":
                break
                # print("Warning: Received \"ok\" during get_config.")
                # continue

            if line[:1] == "$":
                line = line[1:]

                line = line.split("=")

                if len(line) != 2:
                    continue

                config[line[0]] = line[1]
            else:
                print(f"Warning: Received unexpected line during get_config: {line}")

        return config

    def halt(self):
        self.s.write([0x18])
        self.s.flush()

    # def query_response(query, timeout):
    #     self.s.write(query.encode("utf-8"))
    #     self.s.timeout = timeout
    #     response = self.s.readline()
    #     self.s.timeout = None

    #     return response.strip().decode("utf-8")

    # def home(self, block = False):
    def home(self):
        if self.s.in_waiting > 0:
            # TODO How to handle this? Maybe loop "readline" until empty?
            print("Warning: Input buffer not empty.")

        self.s.reset_input_buffer()

        self.s.write(b"\n")

        # self.s.timeout = 0.1
        response = self.s.readline().strip()
        # self.s.timeout = None

        if response != b"ok":
            print("Error: Failed to home:", response)
            return False

        self.s.write(b"$H\n")

        # self.homing = True

        # if block:
        self.s.timeout = None

        response = self.s.readline().strip()

        self.s.timeout = 0.5

        # self.s.write(b"\n")

        # response = self.s.readline()

        if response != b"ok":
            print("Error: Failed to home:", response)
            return False

        return True

    def wait(self):
        while True:
            s = self.send("?")
            if s is None:
                time.sleep(1)
                print("s is None")
                continue

            s = s.decode("utf-8")

            if s[:4] == "<Run":
                time.sleep(1)
            elif s[:5] == "<Idle":
                return
            else:
                print(s)

    def feedrate(self, f):
        # self.send("F{0:.4f}".format(f))
        if type(f) is not int:
            f = int(f)
        self.send("G1 F{}".format(f))

    def reset_coords(self, x, y, z):
        self.position = [x, y, z]
        # self.send("G10 P0 L20 X0 Y0 Z0")
        # print("G10 P0 L20 X{:.4f} Y{:.4f} Z{:.4f}".format(x, y, z))
        self.send("G10 P0 L20 X{:.4f} Y{:.4f} Z{:.4f}".format(x, y, z))

    # def reset_coords(self):
    #     self.position = [0, 0, 0]
    #     # self.send("G92 X{0:.4f} Y{1:.4f} Z{2:.4f}".format(x, y, z))
    #     self.send("G10 L2 P0 X0 Y0 Z0")
    #     self.send("G10 L2 P1 X0 Y0 Z0")
    #     self.send("G10 L2 P2 X0 Y0 Z0")
    #     self.send("G10 L2 P3 X0 Y0 Z0")
    #     self.send("G10 L2 P4 X0 Y0 Z0")
    #     self.send("G10 L2 P5 X0 Y0 Z0")
    #     self.send("G10 L2 P6 X0 Y0 Z0")

    def absolute_coords(self):
        self.absolute = True
        self.send("G90")

    def relative_coords(self):
        self.absolute = False
        self.send("G91")

    # def print_state(self):
    #     self.send("?")

    def goto(self, x, y, z = None, f = None, e = None):
        if self.absolute is None or not self.absolute:
            if not self.send("G90"):
                return False
            self.absolute = True

        s = "G1"

        if x is not None:
            s = s + " X{0:.4f}".format(x)
            self.position[0] = x

        if y is not None:
            s = s + " Y{0:.4f}".format(y)
            self.position[1] = y

        if z is not None:
            s = s + " Z{0:.4f}".format(z)
            self.position[2] = z

        if e is not None:
            s = s + " E{0:.4f}".format(e)

        if f is not None:
            s = s + " F{0:.4f}".format(f)

        return self.send(s)

    def move(self, x, y, z = None, f = None, e = None):
        if self.absolute is None or self.absolute:
            if not self.send("G91"):
                return False
            self.absolute = False

        s = "G1"

        if x is not None:
            s = s + " X{0:.4f}".format(x)
            self.position[0] += x

        if y is not None:
            s = s + " Y{0:.4f}".format(y)
            self.position[1] += y

        if z is not None:
            s = s + " Z{0:.4f}".format(z)
            self.position[2] += z

        if e is not None:
            s = s + " E{0:.4f}".format(e)

        if f is not None:
            s = s + " F{0:.4f}".format(f)

        return self.send(s)

    # def circle(self, diameter, segments = 20):
    #     for i in range(segments):
    #         angle = ((i + 1) / segments) * 2 * pi
    #         x = (diameter / 2) * np.cos(angle)
    #         y = (diameter / 2) * np.sin(angle)

    #         # print(x, y)
    #         self.move(x, y)

    # def circle(center_x, center_y, radius, feed_rate, num_points):
    #     """
    #     Generates G-code for a circle with the given center and radius.

    #     Args:
    #         center_x (float): X-coordinate of the center of the circle.
    #         center_y (float): Y-coordinate of the center of the circle.
    #         radius (float): Radius of the circle.
    #         feed_rate (float): Feed rate of the tool.
    #         num_points (int): Number of points to use when approximating the circle.

    #     Returns:
    #         str: A string containing the G-code for the circle.
    #     """
    #     # gcode = ""
    #     # gcode += "G21\n"  # Set units to millimeters
    #     # gcode += "G90\n"  # Set absolute coordinates

    #     for i in range(num_points):
    #         angle = i * (2 * math.pi / num_points)
    #         x = center_x + radius * math.cos(angle)
    #         y = center_y + radius * math.sin(angle)
    #         if i == 0:
    #             self.goto(x, y)
    #             # gcode += "G0 X{:.3f} Y{:.3f} F{:.1f}\n".format(x, y, feed_rate)
    #         else:
    #             self.goto(x, y)
    #             # gcode += "G1 X{:.3f} Y{:.3f} F{:.1f}\n".format(x, y, feed_rate)

    #     # return gcode

    # Perform a circle around the given center point which is relative to the current position.
    def circle(self, center_x, center_y, num_points, clockwise = True):
        # def distance(x1, y1, x2, y2):
        #     return math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)

        # radius = distance(center_x, center_y, self.position[0], self.position[1])

        # for i in range(num_points):
        #     angle = i * (2 * math.pi / num_points)
        #     x = center_x + radius * math.cos(angle)
        #     y = center_y + radius * math.sin(angle)
        #     print(x, y)
        #     # self.goto(x, y)

        radius = math.sqrt(center_x ** 2 + center_y ** 2)

        start_angle = math.atan2(-center_y, -center_x)

        center_x += self.position[0]
        center_y += self.position[1]

        r = reversed(range(num_points + 1)) if clockwise else range(num_points + 1)

        for i in r:
            angle = start_angle + (i * (2 * math.pi / num_points))
            x = center_x + radius * math.cos(angle)
            y = center_y + radius * math.sin(angle)
            # print(x, y)
            self.goto(x, y)


    # def get_current_position(self):
    #     """
    #     Sends the status report command to a GRBL controller and parses the response
    #     to extract the current position.

    #     Args:
    #         ser (serial.Serial): An open serial port object representing the connection
    #         to the GRBL controller.

    #     Returns:
    #         tuple: A tuple containing the X, Y, and Z coordinates of the current position.
    #     """
    #     self.s.write(b"?")  # Send the status report command
    #     response = self.s.readline().decode().strip()  # Read the response

    #     # Parse the response to extract the current position
    #     position_string = response.split(":")[1].split("|")[0]
    #     position_parts = position_string.split(",")
    #     x = float(position_parts[0])
    #     y = float(position_parts[1])
    #     z = float(position_parts[2])

    #     return x, y, z

    def send(self, line, wait = True):
        # if self.s is None:
        #     print("No serial port")
        #     return

        if len(line) == 0:
            return True

        if line[0] == '%':
            return True

        if line[0] == ';':
            return True

        if line[0] == '(' and line[-1] == ')':
            return True

        if line == "M2":
            return True

        # if self.s is not None:
        self.s.write(str.encode(line + "\n"))

        # print(line)

        while(True):
            if self.s is None:
                return False
                # break

            self.s.timeout = None
            line = self.s.readline();
            self.s.timeout = 0.5
            # print(line)
            # TODO Handle grbl alarms.

            # if line == b'':
            #     continue

            # try:
            #     response = line.strip().decode().split()[0]
            # except IndexError:
            #     print(">", line, "<")
            #     time.sleep(0.1)
            #     continue

            # if response begins with "error"
            # if response.startswith("error"):
            #     print(response)
            #     exit(-1)

            if line == b"ok\r\n":
                # break
                return True
            # elif response == "echo:busy:":
            #     time.sleep(0.1)
            else:
                print(line.strip())
                # self.s.timeout = 0.5
                # print(self.s.readlines())
                # return line
                # self.halt()
                # self.disconnect()
                # break
                return False
                # exit(-1)

        # return True

    def print(self):
        while True:
            line = self.s.readline()
            if line == b'':
                continue
            print(line)

    def spindle(self, speed):
        self.send("S{0:.4f}".format(speed))
        if speed > 0:
            self.send("M3")
        else:
            self.send("M5")

    def delay(self, ms):
        if not instance(ms, int):
            ms = int(ms)

        self.send(f"G4 P{ms}")

    def runFile(self, path):
        absolute = None

        print("; Running file", path)

        try:
            file = open(path, 'r');
        except FileNotFoundError:
            print(f"Error opening gcode path \"{path}\"")
            exit(-1)

        for line in file:
            line = line.strip()

            # print(line)

            # TODO Possibly remove temperature settings.
            semi = line.find(';')

            if semi != -1:
                line = line[:semi]

            # Remove brace comments from line
            openBrace = line.find('(')

            if openBrace != -1:
                closeBrace = line.find(')')
                line = line[:openBrace] + line[closeBrace + 1:]

            if len(line) == 0:
                continue

            ignoreList = ["%", "G64", "T1", "T2"]

            for ignore in ignoreList:
                if line.startswith(ignore):
                    continue

            # if line[0] == '%':
            #     continue

            # if line[0] == ';':
            #     continue

            # if line[0] == '(' and line[-1] == ')':
            #     continue

            # if line.startswith("G64"):
            #     continue

            # if line.startswith("T1"):
            #     continue

            # if line.startswith("T2"):
            #     continue

            # print(oldLine, "->", line)
            self.send(line)

        file.close()
        file = None

# absolute = None

# def runFile(path):
#     global absolute
#     # global file

#     absolute = None

#     print("; Running file", path)

#     try:
#         file = open(path, 'r');
#     except FileNotFoundError:
#         print(f"Error opening gcode path \"{path}\"")
#         exit(-1)

#     for line in file:
#         line = line.strip()
#         # TODO Possibly remove temperature settings.

#         if len(line) == 0:
#             continue

#         # if line[0] == '%':
#         #     continue

#         # if line[0] == ';':
#         #     continue

#         # if line[0] == '(' and line[-1] == ')':
#         #     continue

#         if "M104" in line:
#             continue

#         if "M140" in line:
#             continue

#         if "M109" in line:
#             continue

#         if "M190" in line:
#             continue

#         print(line)

#     file.close()
#     file = None

# def goto(x, y, z, f = None, e = None):
#     global absolute

#     if absolute is None or not absolute:
#         print("G90")
#         absolute = True

#     s = "G1"

#     if x is not None:
#         s = s + " X{0:.4f}".format(x)

#     if y is not None:
#         s = s + " Y{0:.4f}".format(y)

#     if z is not None:
#         s = s + " Z{0:.4f}".format(z)

#     if e is not None:
#         s = s + " E{0:.4f}".format(e)

#     if f is not None:
#         s = s + " F{0:.4f}".format(f)

#     print(s)

# def move(x, y, z, f = None, e = None):
#     global absolute

#     if absolute is None or absolute:
#         print("G91")
#         absolute = False

#     s = "G1"

#     if x is not None:
#         s = s + " X{0:.4f}".format(x)

#     if y is not None:
#         s = s + " Y{0:.4f}".format(y)

#     if z is not None:
#         s = s + " Z{0:.4f}".format(z)

#     if e is not None:
#         s = s + " E{0:.4f}".format(e)

#     if f is not None:
#         s = s + " F{0:.4f}".format(f)

#     print(s)

# def home(s=None):
#     # print("G28")
#     # if s is not None:
#         s.write(b"G28")

# def retract():
#     pass
#     # print("G10")

# def unretract():
#     pass
#     # print("G11")

# def setBedTemp(t):
#     print("M140 S" + str(t) + " ; Set bed temperature to " + str(t))

# def setNozzleTemp(t):
#     print("M104 S" + str(t) + " ; Set nozzle temperature to " + str(t))

# def waitBedTemp(t):
#     print("M190 S" + str(t) + " ; Wait for bed temperature to reach at least " + str(t))

# def waitNozzleTemp(t):
#     print("M109 S" + str(t) + " ; Wait for nozzle temperature to reach at least " + str(t))

# def setFan(percent):
#     value = int(percent * 255 / 100)
#     print("M106 S" + str(value) + " ; Set fan to " + str(percent) + "%")

# def disableMotors():
#     print("M84 X Y Z E ; Disable motors")
